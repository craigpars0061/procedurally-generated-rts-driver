<?php
require_once BASEDIR . '/includes/autoload.php';

if (!defined('MYACTIVERECORD_CONNECTION_STR')) {
    define('MYACTIVERECORD_CONNECTION_STR', 'mysql://root:1@localhost/savestate');
}

/**
 * Extended this class to customize.
 * This class is responsible for extending MyActiveRecord.
 * This allows me to add functionality to MyActiveRecord but allow me to upgrade MyActiveRecord if I need to.
 * Object/Relational Mapper for MySQL.
 *
 * @category    Database
 * @package     MyActiveRecord
 * @author      Craig Parsons
 * @copyright   2014
 * @version     1.0
 */
class MoreActiveRecord extends MyActiveRecord
{

    /**
     * Returns true if errors exist
     *
     * @return boolean False if there 1 or more errors
     *                 False if there are no errors.
     */
    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    /**
     * FindAll description
     * Since we can do late static binding now. We don't have to pass in the class name. eg:
     * <code>
     * $cars = MyActiveRecord::FindAll('Car');
     * $cars = MyActiveRecord::FindAll('Car', "colour='red'", 'make ASC', 10);
     * </code>
     * We can now do it this way
     *
     * <code>
     * $cars = Car::FindAll();
     * $cars = MyActiveRecord::FindAll('Car', "colour='red'", 'make ASC', 10);
     * </code>
     *
     * @param string $strClassName The name of the class for which you want objects
     * @param mixed  $mxdWhere     Optional SQL WHERE fragment, eg: "username='fred' AND password='123'"
     *                             can also be expressed as array, e.g. array( 'username'=>'fred', password=>'123')
     * @param string  strOrderBy  optional SQL ORDER BY fragment, eg: "username ASC"
     * @param string  intLimit    optional integer limiting the number of records returned
     * @param string  intOffset   optional integer to offset the first record brought back
     * @param string  strGroupBy  optional GROUP BY fragment
     *
     * @return array Array of objects. Array is empty if no objects found
     */
    public function findAll($strClassName = null, $mxdWhere = null, $strOrderBy = 'id ASC', $intLimit = DEFAULTLIMIT, $intOffset = 0)
    {
        // If the class name wasn't passed in then go get it.
        if (is_null($strClassName) === true) {
            $strClassName = get_called_class();
        }

        return parent::FindAll($strClassName, $mxdWhere, $strOrderBy, $intLimit, $intOffset);
    }

    /**
     * Returns an object of class strClass found in database with a specific
     * integer ID. An array of integers can be passed in order to retrieve an
     * array of objects with matching IDs
     * eg:
     * <code>
     * $car = MyActiveRecord::FindById(15);
     * $cars = MyActiveRecord::FindById(3, 5, 13);
     * </code>
     *
     * @static
     * @param string  strClass The name of the class for which you want objects
     * @param mixed   mxdID    integer or array of integers
     *
     * @return mixed object, or array of objects
     */
    public function findById($mxdID, $strClass = null)
    {
        if (is_null($strClassName) === true) {
            $strClassName = get_called_class();
        }
        $primaryKey = 'id';

        if (is_array($mxdID)) {
            $idlist = implode(', ', $mxdID);
            $idlist = (count($mxdID) > 0) ? $idlist : '0';

            return MoreActiveRecord::FindAll($strClassName, '`' . $primaryKey . '` IN ( ' . $idlist . ' )');
        } else {
            $arrWhere = array(
                $primaryKey => intval($mxdID)
            );
            $strOrderBy = 'id ASC';
            $arrObjects = self::findAll($strClassName, $arrWhere, $strOrderBy, 1);

            return array_shift($arrObjects);
        }
    }

    /**
     * Returns the first object of class strClass found in database
     * optional where, order and limit paramaters enable the results to be
     * narrowed down
     * usage
     * eg:
     * <code>
     * $car = MyActiveRecord::FindFirst('Car', "colour='red'", 'model ASC');
     * </code>
     * or
     * <code>
     * $car = Car::FindFirst();
     * </code>
     *
     * @static
     *
     * @param strClassName  string, the name of the class for which you want objects
     * @param strWhere  optional SQL WHERE argument, eg: "username='fred' AND password='123'"
     * @param strOrderBy  optional SQL ORDER BY argument, eg: "username ASC"
     *
     * @return object, false if no objects found
     */
    public function findFirst($strClassName = null, $strWhere = null, $strOrderBy = 'id ASC')
    {
        // If the class name wasn't passed in then go get it.
        if (is_null($strClassName) === true) {
            $strClassName = get_called_class();
        }

        $arrObjects = self::findAll($strClassName, $strWhere, $strOrderBy, 1);
        if (count($arrObjects)) {
            return array_shift($arrObjects);
        } else {
            return false;
        }
    }

    /**
     * Returns an array describing the specified table, or false if the table
     * does not exist in the database.
     * The array contains one array per database column, keyed by the column
     * name. To see the structure of the array you could try:
     * <code>
     * printr( MyActiveRecord::Columns('yourtable') );
     * </code>
     *
     * @static
     * @param string strTable The name of the database table
     * @return array Table columns. False if the table does not exist.
     */
    public function columns($strTable)
    {
        // Cache results locally.
        static $arrCache = array();

        // If this is already cached?
        // Then return the result previously stored in the columns array.
        if (isset($arrCache[$strTable])) {
            return $arrCache[$strTable];

        } else {

            // Run 'describe' query to get the columns in the table.
            if ($queryResult = self::Query("SHOW COLUMNS FROM " . $strTable)) {
                $arrFields = array();

                while ($arrCol = mysql_fetch_assoc($queryResult)) {
                    $arrFields[$arrCol['Field']] = $arrCol;
                }

                mysql_free_result($queryResult);

                // Cache results for future use and return.
                return $arrCache[$strTable] = $arrFields;

            } else {
                trigger_error("MoreActiveRecord::Columns() - could not decribe table " . $strTable, E_USER_WARNING);

                return false;
            }
        }
    }

    /**
     * classNameToTableName
     * Had to create this function because class2Table wouldn't work.
     *
     * @param string $strClass
     *
     * @return
     */
    public function classNameToTableName($mxdClass)
    {

        $origClass = is_object($mxdClass) ? get_class($mxdClass) : $mxdClass;
        class_exists($origClass) or trigger_error("MyActiveRecord::Class2Table - Class $origClass does not exist", E_USER_ERROR);
        $class = $origClass;

        // This is to deal with name spaces.
        if (strpos($class, '\\') !== false) {
            $class = array_pop(explode('\\', $class));
        }

        while (!self::TableExists(strtolower($class)) && $class != 'MyActiveRecord') {
            $class = get_parent_class($class);
        }

        $table = strtolower($class);
        if ($table == 'myactiverecord') {
            trigger_error("MyActiveRecord::Class2Table - Class $origClass does not have a table representation", E_USER_ERROR);

            return false;
        }

        return $table;
    }

    /**
     * Declares and object and then populates it
     * Very usefull to use in a loop when you mysql fetched from associative
     *
     * @param string strclass The class name of the objects were going to declare
     */
    public function declareAndPopulate($strClass, $arrValues = null)
    {
        $objDatabaseRelationalMapping = new $strClass();
        $strTable                     = MoreActiveRecord::classNameToTableName($strClass);

        // Expecting arrvalues to come from a "$arrvalues = mysqlfetchassoc($result)" call.
        foreach (MoreActiveRecord::columns($strTable) as $key => $field) {

            if ($field['Default']) {
                $objDatabaseRelationalMapping->$key = $field['Default'];
            }
        }

        $objDatabaseRelationalMapping->populate($arrValues);

        return $objDatabaseRelationalMapping;
    }

    /**
     * Saves the object back to the database
     * eg:
     * <code>
     * $car = MyActiveRecord::Create('Car');
     * print $car->id;  // NULL
     * $car->save();
     * print $car->id;  // 1
     * </code>
     *
     * NB: if the object has registered errors, save() will return false
     * without attempting to save the object to the database
     *
     * @return boolean true on success false on fail
     */
    public function save()
    {
        // if this object has registered errors, we back off and return false.
        if ($this->get_errors()) {
            return false;

        } else {
            $table = self::classNameToTableName(get_class($this));

            // check for single-table-inheritance
            if (strtolower(get_class($this)) != $table) {
                $this->class = get_class($this);
            }

            $fields = self::Columns($table);
            // sort out key and value pairs
            foreach ($fields as $key => $field) {
                if ($key != $this->_primary_key) {
                    $val    = self::Escape(isset($this->$key) ? $this->$key : null);
                    $vals[] = $val;
                    $keys[] = "`" . $key . "`";
                    $set[]  = "`$key` = $val";
                }
            }

            // Insert or update as required
            $k = $this->_primary_key;
            if (isset($this->$k) && $this->$k > 0) {
                $sql = "UPDATE `$table` SET " . implode($set, ", ") . ' WHERE `' . $k . '` = ' . $this->$k;
            } else {
                $sql = "INSERT INTO `$table` (" . implode($keys, ", ") . ") VALUES (" . implode($vals, ", ") . ")";
            }
            $success = self::Query($sql);

            if (!isset($this->$k) || $this->$k == 0) {
                $this->$k = mysql_insert_id(self::Connection());
            }

            return $success;
        }
    }
}
