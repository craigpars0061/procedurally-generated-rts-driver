<?php

//Bring it completely down over here, when you don't want any output showing at all by syntax errors in classes
//todo: set up static class for the usual globals
/**
 *if( $BringItDown == true )
 *{
 *    die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transistional.dtd"<html><head NULL></head><body bgcolor="Black">
 *
 *            <div style="margin-right: auto; text-align:center; margin-left: auto; width: 100%;">
 *              <div style="text-align:center;margin-right: auto; margin-left: auto;">
 *                  <IMG SRC="rubber-bandit-gun-01.jpg" WIDTH="540" HEIGHT="359">
 *              </div>
 *              <div style="text-align:center;color:#FFFFFF;margin-top: 20px;"><h2>Elastic Gun</hd></div>
 *                <div style="text-align:center;color:#FFFFFF;margin-right: auto; margin-left: auto;font-family: Helvetica, Geneva, Arial, sans-serif;">
 *                  Under construction
 *                  <br>
 *             </div>
 *
 *              </div>
 *            </div>
 *    </body></html>');
 *}
 */
define('MY_ACTIVE_ACTIVE_RECORD_VERSION', '0.6');

require_once(BASEDIR . "/includes/tools.php");

/**
 * Debug Help
 */
class DebugHelp
{
    /**
     * I use this array to display human readable descriptions of error types
     * given an errno.
     *
     * @var array
     */
    protected $arrErrorDescriptions;

    /**
     * Creates an instance of this object.
     */
    public function __construct()
    {

      // Initialize errorno descriptions.
      $this->arrErrorDescriptions = array(
            E_ERROR => 'error',
            E_WARNING => 'warning',
            E_PARSE => 'parsing error',
            E_NOTICE => 'notice',
            E_CORE_ERROR => 'core error',
            E_CORE_WARNING => 'core warning',
            E_COMPILE_ERROR => 'compile error',
            E_COMPILE_WARNING => 'compile warning',
            E_USER_ERROR => 'user error',
            E_USER_WARNING => 'user warning',
            E_USER_NOTICE => 'User notice',
            E_STRICT => 'Strict'
        );
    }

    /**
     * debugB description
     *
     * @param string $str_information_xtra
     *
     * @return void
     */
    public function debugB($str_information_xtra = '')
    {
        $str_information_xtra .= '
    ' . $this->returnStringPrettyStackTrace();
        self::debugCubeEncasement($str_information_xtra);
    }

    /**
     * Displays info in a pretty box.
     *
     * @param mixed  $str_information Array or string or Object.
     * @param string $str_color       The color you want in the div.
     *
     * @return void
     */
    public function debugCubeEncasement($str_information, $str_color = '#000066')
    {
        print('<div style="margin-top:0;margin-right:auto;margin-left:10px;border:2px solid ' . $str_color . '"><pre>');

        // Print anything.
        self::printA($str_information);

        print("</div></pre>");
    }

    /**
     * Displays the request array.
     *
     * @return void
     */
    public function displayRequestArray()
    {
        echo '<pre>Request ';
        print_r($_REQUEST);
        echo '</pre>';

    } //end displayRequestArray()

    /**
     * Surround what you want to print with pre tags.
     *
     * @param $var mixed array or other $var.
     *
     * @return none
     */
    public function prettyPrint($var)
    {
        echo '<pre>';
        if ((is_array($var) === true) || (is_object($var) === true)) {
            print_r($var);

        } else {
            echo $var;
        }

        echo '</pre>';

    } //end prettyPrint()

    /**
     * Tracing tool
     * Use with string replace to help trace where the code goes to.
     *
     * @param Boolean $DontDie
     * @param String  $PrintMe
     */
    public function DisplayLineThenDie($Die = true, $mxdPrintMe = NULL)
    {
        foreach (debug_backtrace() as $trace)
            echo "<pre>File:" . $trace['file'] . "<br> From Function: " . $trace['function'] . "<br> Line Number: " . $trace['line'];

        if (isset($mxdPrintMe))
            print("
  [" . $mxdPrintMe . "]");

        else if (is_array($mxdPrintMe)) {
            print_r($mxdPrintMe);
        }

        if ($Die) {
            die("<br />...<br />Oh no! I died! :'(</pre>");
        }

        print('</pre>');
    }

    /**
     * [debug_display_error description]
     *
     * @param [type] $arr_args [description]
     *
     * @return [type] [description]
     */
    public function debug_display_error($arr_args)
    {
        ob_start('mb_output_handler');
        echo '
    <div style="background-color:#DCB3B4;text-align:left;padding:25px;margin-left:10px;">';
        echo '
        <hr>
        <br>';
        DisplayLineThenDie(false, implode('
          ', $arr_args));
        echo '
    </div>';

        trigger_error("error: " . ob_get_clean() . "<br>", E_USER_WARNING); //E_USER_WARNING
    }

    /**
     * Print anything
     *
     * @param mixed $mxdPrintMe array or string
     *
     * @return void
     */
    public function printA($mxdPrintMe = NULL)
    {
        if (isset($mxdPrintMe)) {
            if (is_array($mxdPrintMe) || is_object($mxdPrintMe)) {
                print_r($mxdPrintMe);
            } else {
                print($mxdPrintMe);
            }
        }
    }

    /**
     *
     * @param integer $errno      A number representing what type of error occurred.
     * @param string  $errstr     A description of the error that occurred.
     * @param sting   $errfile    A filename of where the error that occurred.
     * @param integer $errline    A line number of where the error occurred.
     * @param string  $errcontext [description]
     *
     * @return void
     */
    public function errorHandler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        $errortype = $this->arrErrorDescriptions;

        //Don't display these erros, If you want to Notices and Strict then comment out these next three lines.
        if ($errno == E_NOTICE || $errno == 8192 || $errno == E_STRICT) {
            return;
        }

        if (isset($errortype[(int) $errno])) {
            echo '
 Error type [<span style="color:#8C001A">' . $errortype[$errno];
        echo '</span>]';

        } else {
            echo "
            Unknown error type.";
        }
        echo '  <span style="color:#8C001A">' . print_r($errstr, true) . '</span>';
        echo '  ' . get_class($this) . ' :: ' . __FUNCTION__ . '() at line ' . __LINE__;
        echo "\n\n  ERRNO " . print_r($errno, true);

        echo "\n\n  ---ERRSTR---\n  " . '<span style="color:#8C001A">' . print_r($errstr, true) . '</span>';
        echo "\n\n  ---ERRFILE---\n  " . print_r($errfile, true);
        echo "\n\n  ---ERRLINE---\n  " . print_r($errline, true);
        echo '<pre>';
        self::prettyStackTrace();
        echo '</pre>';
    }

    /**
     * Called when an exception is thrown.
     *
     * @param [type] $exception [description]
     *
     * @return [type] [description]
     */
    public function exceptionHandler($exception)
    {
        // These are our templates.
        $traceline = "#%s %s(%s): %s(%s)";
        $msg       = "PHP Fatal error:  Uncaught exception '%s' with message '%s' in %s:%s\nStack trace:\n%s\n  thrown in %s on line %s";

        // alter your trace as you please, here
        $trace = $exception->getTrace();
        foreach ($trace as $key => $stackPoint) {
            // I'm converting arguments to their type
            // (prevents passwords from ever getting logged as anything other than 'string')
            $trace[$key]['args'] = array_map('gettype', $trace[$key]['args']);
        }

        // Build your tracelines.
        $result = array();
        foreach ($trace as $key => $stackPoint) {
            $result[] = sprintf($traceline, $key, $stackPoint['file'], $stackPoint['line'], $stackPoint['function'], implode(', ', $stackPoint['args']));
        }

        // Trace always ends with {main}
        $result[] = '#' . ++$key . ' {main}';

        // write tracelines into main template
        $msg = sprintf($msg, get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine(), implode("\n", $result), $exception->getFile(), $exception->getLine());

        // log or echo as you please
        print($msg);
    }

    /**
     * You can find where something is being called from
     *
     * @param boolean $bool_shift_first_element [description]
     * @param [type]  $StackTrace               [description]
     *
     * @return [type] [description]
     */
    public function prettyStackTrace($bool_shift_first_element = false, $StackTrace = NULL)
    {
        echo '<br><br> Stack Trace:<br><br>';

        if (is_null($StackTrace)) {
            $StackTrace = debug_backtrace();
        }

        // For when errors are being display via sycle_quer.
        // Makes sure the last call on the stack doesn't show up since it's known the last call was sycle_query.
        // Turned off by default.
        if ($bool_shift_first_element) {
            array_shift($StackTrace);
            echo 'array shift';
        }

        foreach ($StackTrace as $trace) {

            if (isset($trace['file'])) {

                print("  File:" . $trace['file'] . "<br />");
                print("  From Function: " . $trace['function'] . "<br>");
                print("  Line Number:" . $trace['line'] . "<br />
");
            }
        }
        echo ' <br>';

    }//end prettyStackTrace

    /**
     * Calls a display function and returns its output as a string.
     *
     * @param [type] $display_function_name [description]
     * @param [type] $arr_args              [description]
     *
     * @return String
     */
    public function returnString($display_function_name, $arr_args)
    {
        // Start the output buffer and capture all the printing of function called on the following line.
        ob_start('mb_output_handler');

        // Call the display funciton.
        // The following line does this "$this->$$display_function_name();" sort of, but passes in that array as arguements.
        call_user_func_array(array(
            $this,
            $display_function_name
        ), $arr_args);

        // Return that string.
        return ob_get_clean();
    }

    /**
     * Override call behavior to add returnString functions
     * is triggered when invoking inaccessible methods in an object context.
     * If you call any function with the returnString prefix
     *
     * @param [type] $function_name
     * @param array  $arr_args
     *
     * @return [type] [description]
     */
    public function __call($function_name, $arr_args = array())
    {
        $str_prefix = 'returnString';

        // If the beginning of this method begins with returnString then call returnString with the function name
        if (strpos($function_name, $str_prefix) === 0) {
            $display_function_name = str_replace($str_prefix, '', $function_name);

            if (method_exists($this, $display_function_name) === true) {
                return $this->returnString($display_function_name, $arr_args);
            }
        }
    }//end __call

} //end DebugHelp

/**
 * So that you can find where something is being called from.
 *
 * @param boolean $boolShiftFirstElement
 * @param array   $StackTrace
 *
 * @return void
 */
function prettyStackTrace($boolShiftFirstElement = false, $StackTrace = NULL)
{

    DebugHelp::prettyStackTrace($boolShiftFirstElement, $StackTrace);

}

/**
 * This function is called by handleError in Sycle's common.inc.
 *
 * @param array $arguments
 *
 * @return void
 */
function debug_display_error($arguments)
{
    DebugHelp::debug_display_error($arguments);
}

/**
 * Print anything
 *
 * @param  mixed $mxdPrintMe  array or string
 *
 * @return void
 */
function printA($mxdPrintMe = NULL)
{
    DebugHelp::printA();
}

/**
 * Print in anything with a stacktace
 *
 * @param mixed $strInformationExtra
 * 
 * @return void
 */
function debugB($strInformationExtra)
{
    $debugHelp = new DebugHelp();
    $debugHelp->debugB($strInformationExtra);
}

/**
 * Setting the error Handler to use DebugHelp
 *
 * @param  integer $errno      A number representing what type of error occurred.
 * @param  string  $errstr     A description of the error that occurred.
 * @param  sting   $errfile    A filename of where the error that occurred.
 * @param  integer $errline    A line number of where the error occurred.
 * @param  string  $errcontext
 *
 * @return void
 */
function errorHandler($errno, $errstr, $errfile, $errline, $errcontext)
{
    $debugHelp = new DebugHelp();

    // Returning the string from returnStringErrorHanler.
    $strMakeMePretty = $debugHelp->returnStringErrorHandler($errno, $errstr, $errfile, $errline, $errcontext);
    if ($strMakeMePretty) {
        DebugHelp::debugCubeEncasement($strMakeMePretty);
    }
}

/**
 * Setting the exception Handler to use DebugHelp
 *
 * @param object $exception
 *
 * @return void
 */
function exceptionHandler($exception)
{
    $debugHelp = new DebugHelp();
    DebugHelp::debugCubeEncasement($this->returnStringExceptionHandler($exception));
}

/**
 * If this file is included, whenever someone executes:i.e. new Object()
 * If the required file isn't included then it will come here first and run the autoload function
 * in order to try to include the correct file.
 *
 * @author Craig Parsons <CraigPars0061@gmail.com>
 * @param string $className the class name of the object you attempted to construct
 */
spl_autoload_register(function($className) {

    // this is for anything that you want to catch before you go through all the other extra work.
    switch ($className) {
        case "TileType":
            require_once BASEDIR . '/includes/model/Database/' . $className . '.php';

            return 0;
        case "MyActiveRecord":
            require_once(BASEDIR . "/includes/" . $className . "." . MY_ACTIVE_ACTIVE_RECORD_VERSION . ".php");

            return 0;
        case "CascadingStyleSheetLanguageContainer":
            require_once(BASEDIR . "/includes/framework/$className.php");

            return 0;
        case "JavascriptLanguageContainer":
            require_once(BASEDIR . "/includes/framework/$className.php");

            return 0;
    }
    $NameSpacePieces = explode('\\', $className);

    if (count($NameSpacePieces) > 1) {
        array_shift($NameSpacePieces);
        require_once BASEDIR . "/includes/".implode($NameSpacePieces, '/').'.php';

        return 0;
    }

    $UnderscorePieces = explode('_', $className);

    $CamelCasePieces = explodeCase($className);

    // If there aren't any underscores in the name then procede normally.
    if (count($UnderscorePieces) > 1) {
        $UnderscorePrefix = $UnderscorePieces[0];

        // All the smarty system plugins are in this directory.
        switch ($UnderscorePrefix) {
            case "smarty":
            case "Smarty":
                require_once(BASEDIR . '/Smarty-3.0.5/libs/sysplugins/' . strtolower($className) . '.php');
                break;
            default:
                require_once(BASEDIR . "/includes/" . $className . ".php");
                break;
        }

        // If the camel case explode function determines that there are more than 1 word concatenated together.
    } elseif (count($CamelCasePieces) > 1) {

        // Then grab the suffix to determine which folder it will be in.
        $CamelCaseSuffix = end($CamelCasePieces);

        // Grab the prefix in case its abstract.
        $CamelCasePrefix = $CamelCasePieces[0];

        switch ($CamelCasePrefix) {
            case "abstract":
                require_once(BASEDIR . "/includes/foundation/" . $className . ".php");
                break;
            default:
                // Don't do anthing the next switch statement still needs to check for Suffix's.
                // Only add Cases for the exceptions that don't comply with the default.
                switch ($CamelCaseSuffix) {
                    case "view":
                        require_once(BASEDIR . "/includes/view/" . $className . ".php");
                        break;
                    case "element":
                        require_once(BASEDIR . "/includes/elements/" . $className . ".php");
                        break;
                    case "adapter":
                        require_once(BASEDIR . "/includes/adapters/" . $className . ".php");
                        break;
                    default:
                        require_once(BASEDIR . "/includes/" . $className . ".php");
                        break;
                }
                break;
        }

    } else {

        // This is for Non-CamelCases and Non-Underscore seperated class titles.
        // Only add Cases for the exceptions that don't comply with the default.
        switch ($className) {

            case "Smarty":
            case "smarty":
                require_once(BASEDIR . '/Smarty-3.0.5/libs/Smarty.php');
                break;

            default:
                require_once(BASEDIR . "/includes/$className.php");
                break;
        }
    }
});
