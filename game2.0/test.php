<?php

$list = array(
    '1' => 'inner-Land',
    '2' => 'inner-Rock',
    '3' => 'inner-WaterTile',
    '4' => 'TopRightConvexedCorner-CliffSideTile',
    '5' => 'BottomLeftConvexedCorner-CliffSideTile',
    '6' => 'TopLeftConvexedCorner-CliffSideTile',
    '7' => 'BottomRightConvexedCorner-CliffSideTile',
    '8' => 'TopRightConcaveCorner-CliffSideTile',
    '9' => 'TopLeftConcaveCorner-CliffSideTile',
    '10' => 'bottomRightConcaveCorner-CliffSideTile',
    '11' => 'bottomLeftConcaveCorner-CliffSideTile',
    '12' => 'topEdge-CliffSideTile',
    '13' => 'rightEdge-CliffSideTile',
    '14' => 'bottomEdge-CliffSideTile',
    '15' => 'leftEdge-CliffSideTile',
    '16' => 'TopRightConvexedCorner-WaterTile',
    '17' => 'BottomLeftConvexedCorner-WaterTile',
    '18' => 'TopLeftConvexedCorner-WaterTile',
    '19' => 'BottomRightConvexedCorner-WaterTile',
    '20' => 'TopRightConcaveCorner-WaterTile',
    '21' => 'TopLeftConcaveCorner-WaterTile',
    '22' => 'bottomRightConcaveCorner-WaterTile',
    '23' => 'bottomLeftConcaveCorner-WaterTile',
    '24' => 'topEdge-WaterTile',
    '25' => 'rightEdge-WaterTile',
    '26' => 'bottomEdge-WaterTile',
    '27' => 'leftEdge-WaterTile',
    '29' => 'inner-Tree'
);

foreach($list as $index => $value) {
    echo "'$value' => '$index',<br/>";
}