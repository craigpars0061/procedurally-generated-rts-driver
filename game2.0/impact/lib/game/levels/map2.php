<?php
// Environment settings.
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', '1');

$tilesFromDatabase = array(
    '1' => 'inner-Land',
    '2' => 'inner-Rock',
    '3' => 'inner-WaterTile',
    '4' => 'TopRightConvexedCorner-CliffSideTile',
    '5' => 'BottomLeftConvexedCorner-CliffSideTile',
    '6' => 'TopLeftConvexedCorner-CliffSideTile',
    '7' => 'BottomRightConvexedCorner-CliffSideTile',
    '8' => 'TopRightConcaveCorner-CliffSideTile',
    '9' => 'TopLeftConcaveCorner-CliffSideTile',
    '10' => 'bottomRightConcaveCorner-CliffSideTile',
    '11' => 'bottomLeftConcaveCorner-CliffSideTile',
    '12' => 'topEdge-CliffSideTile',
    '13' => 'rightEdge-CliffSideTile',
    '14' => 'bottomEdge-CliffSideTile',
    '15' => 'leftEdge-CliffSideTile',
    '16' => 'TopRightConvexedCorner-WaterTile',
    '17' => 'BottomLeftConvexedCorner-WaterTile',
    '18' => 'TopLeftConvexedCorner-WaterTile',
    '19' => 'BottomRightConvexedCorner-WaterTile',
    '20' => 'TopRightConcaveCorner-WaterTile',
    '21' => 'TopLeftConcaveCorner-WaterTile',
    '22' => 'bottomRightConcaveCorner-WaterTile',
    '23' => 'bottomLeftConcaveCorner-WaterTile',
    '24' => 'topEdge-WaterTile',
    '25' => 'rightEdge-WaterTile',
    '26' => 'bottomEdge-WaterTile',
    '27' => 'leftEdge-WaterTile',
    '29' => 'inner-Tree'
);

$tilesToTileNumber = array(
    'inner-Land' => '41',
    'inner-Rock' => '42',
    'inner-WaterTile' => '43',
    'TopRightConvexedCorner-CliffSideTile' => '24',
    'BottomLeftConvexedCorner-CliffSideTile' => '28',
    'TopLeftConvexedCorner-CliffSideTile' => '23',
    'BottomRightConvexedCorner-CliffSideTile' => '29',
    'TopRightConcaveCorner-CliffSideTile' => '22',
    'TopLeftConcaveCorner-CliffSideTile' => '21',
    'bottomRightConcaveCorner-CliffSideTile' => '27',
    'bottomLeftConcaveCorner-CliffSideTile' => '26',
    'topEdge-CliffSideTile' => '32',
    'rightEdge-CliffSideTile' => '34',
    'bottomEdge-CliffSideTile' => '37',
    'leftEdge-CliffSideTile' => '25',
    'TopRightConvexedCorner-WaterTile' => '13',
    'BottomLeftConvexedCorner-WaterTile' => '16', 
    'TopLeftConvexedCorner-WaterTile' => '11',
    'BottomRightConvexedCorner-WaterTile' => '18',
    'TopRightConcaveCorner-WaterTile' => '19',
    'TopLeftConcaveCorner-WaterTile' => '20',
    'bottomRightConcaveCorner-WaterTile' => '14 ',
    'bottomLeftConcaveCorner-WaterTile' => '15',
    'topEdge-WaterTile' => '12',
    'rightEdge-WaterTile' => '10',
    'bottomEdge-WaterTile' => '17',
    'leftEdge-WaterTile' => '5',
    'inner-Tree' => '45'
);

$tileTypeIdToTileNumber = array(
    1 => 41,
    2 => 42,
    3 => 43
);


$mysqli = new mysqli('localhost', 'root', 1, 'savestate');

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = '
select * from
tiles;
';

$allTiles = array();

echo '<pre>';
if ($result = $mysqli->query($query)) {

    // Fetch associative array.
    while ($row = $result->fetch_assoc()) {
        $allTiles[$row['mapCoordinateX']][$row['mapCoordinateY']] = $row['tileType_id'];
    }

    // Free result set.
    $result->free();
}

// Close connection.
$mysqli->close();


header('Content-type: text/javascript');
?>
ig.module( 'game.levels.map1' )
.requires( 'impact.image' )
.defines(function(){
LevelMap1=/*JSON[*/{
    "entities": [],
    "layer": [
        {
            "name": "layer1",
            "width": 50,
            "height": 50,
            "linkWithCollision": false,
            "visible": 1,
            "tilesetName": "media/tileset.png",
            "repeat": false,
            "preRender": false,
            "distance": "1",
            "tilesize": 32,
            "foreground": false,
            "data": [
<?php
$size = 50;
for ($intYaxisCoordinate = $size; $intYaxisCoordinate > -1; $intYaxisCoordinate--) {

    echo '[';
    // The rows still go right to left.
    for ($intXaxisCoordinate = 0; $intXaxisCoordinate < $size; $intXaxisCoordinate += 1) {

        if (isset($allTiles[$intXaxisCoordinate][$intYaxisCoordinate])) {
            $currentTile = $allTiles[$intXaxisCoordinate][$intYaxisCoordinate];
            echo $tilesToTileNumber[$tilesFromDatabase[$currentTile]];
            if ($intXaxisCoordinate != ($size-1)) {
                echo ',';
            }
        }
    }
    echo ']';
    // Don't put a comma on the last line.
    if ($intYaxisCoordinate != 0) {
        echo ',';
    }
}
?>
            ]
        }
    ]
}/*]JSON*/;
LevelMap1Resources=[new ig.Image('media/tileset.png')];
});
